package br.ifsc.edu.notas;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    SQLiteDatabase bd;
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bd = openOrCreateDatabase("meubd",MODE_PRIVATE, null);

        bd.execSQL("CREATE TABLE IF NOT EXISTS Notas("+
                "id integer primary key autoincrement,"+
                "titulo varchar not null,"+
                "texto varchar);");

       // bd.execSQL("INSERT INTO Notas (id,titulo,texto) VALUES (NULL,\"Felicidade\",\"\");");

        ContentValues contentValues = new ContentValues();
        contentValues.put("titulo","Agora estou mais feliz");
        contentValues.put("texto","Hoje eu acordei feliz porque vou programar");

        bd.insert("Notas",null,contentValues);

       Cursor cursor = bd.rawQuery("SELECT * FROM Notas", null,null);
       cursor.moveToFirst();
        ArrayList arrayList = new ArrayList();

       String id, titulo,texto;

       while(!cursor.isAfterLast()) {
         //  id = cursor.getString(cursor.getColumnIndex("id"));
           titulo = cursor.getString(cursor.getColumnIndex("titulo"));
         //  texto = cursor.getString(cursor.getColumnIndex("texto"));
         //  Log.d("TabelaNotas", id + titulo + texto);

           arrayList.add(titulo);
           cursor.moveToNext();
       }

       listView = findViewById(R.id.listview);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_list_item_1,
                android.R.id.text1, arrayList
                );

        listView.setAdapter(adapter);

    }
}
